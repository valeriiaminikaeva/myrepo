package laba9Task1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class ActionsTest {
  @Test void tryMultiplication() throws NotNsizeMatrixException {
     int[][] target = new int[][]{{-2,86}, {11,58}};
     Matrix m1 = new Matrix(2,2);
     int[][] numbers1 = new int[][]{{10,4},{8,5}};
     m1.setMatrix(numbers1);
     Matrix m2 = new Matrix(2,2);
     int [][] numbers2 = new int[][]{{-3,11},{7,-6}};
     m2.setMatrix(numbers2);
     assertArrayEquals(target, Actions.multiplication(m1, m2), "tryMultiplication is not correct");
   }

   @Test void tryMultiplication4x3And3X2() throws NotNsizeMatrixException {
      int[][] target = new int[][]{{11,11}, {4,8}, {6,9}, {3,1}};
      Matrix m1 = new Matrix(4,3);
      int[][] numbers1 = new int[][]{{1,0,2},{0,4,0},{0,2,1},{1,0,0}};
      m1.setMatrix(numbers1);
      Matrix m2 = new Matrix(3,2);
      int [][] numbers2 = new int[][]{{3,1},{1,2},{4,5}};
      m2.setMatrix(numbers2);
      assertArrayEquals(target, Actions.multiplication(m1, m2), "tryMultiplication3x4And3X2 is not correct");
    }

   @Test void tryAdding() throws NotNsizeMatrixException {
      int[][] target = new int[][]{{7,15}, {15,-1}};
      Matrix m1 = new Matrix(2,2);
      int[][] numbers1 = new int[][]{{10,4},{8,5}};
      m1.setMatrix(numbers1);
      Matrix m2 = new Matrix(2,2);
      int [][] numbers2 = new int[][]{{-3,11},{7,-6}};
      m2.setMatrix(numbers2);
      assertArrayEquals(target, Actions.sum(m1, m2), "tryAdding is not correct");
    }

    @Test void trySubstraction() throws NotNsizeMatrixException {
       int[][] target = new int[][]{{13,-7}, {1,11}};
       Matrix m1 = new Matrix(2,2);
       int[][] numbers1 = new int[][]{{10,4},{8,5}};
       m1.setMatrix(numbers1);
       Matrix m2 = new Matrix(2,2);
       int [][] numbers2 = new int[][]{{-3,11},{7,-6}};
       m2.setMatrix(numbers2);
       assertArrayEquals(target, Actions.substraction(m1, m2), "trySubstraction is not correct");
     }
}
