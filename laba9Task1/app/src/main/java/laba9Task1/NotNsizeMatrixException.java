package laba9Task1;

class NotNsizeMatrixException extends Exception{
  String message;

  public NotNsizeMatrixException(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }
}
