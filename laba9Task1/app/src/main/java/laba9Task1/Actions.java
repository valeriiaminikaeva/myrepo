package laba9Task1;

public class Actions {

  public static int[][] substraction(Matrix m1, Matrix m2) throws NullPointerException, NotNsizeMatrixException {
    int[][] result;
    int[][] matrix1 = m1.getMatrix();
    int[][] matrix2 = m2.getMatrix();
    if (matrix1 == null || matrix2 == null) {
      throw new NullPointerException("Матрицы незаполнены");
    }
    if (matrix1.length != matrix2.length || matrix1[0].length != matrix2[0].length) {
      throw new  NotNsizeMatrixException("Матрицы не одинакового размера");
    }
    result = new int[matrix1.length][matrix1[0].length];
    for (int i = 0; i < matrix1.length; i++) {
      for (int j = 0; j < matrix1[0].length; j++) {
        result[i][j] = matrix1[i][j] - matrix2[i][j];
      }
    }
    return result;
  }

  public static int[][] sum(Matrix m1, Matrix m2) throws NullPointerException, NotNsizeMatrixException {
    int[][] result;
    int[][] matrix1 = m1.getMatrix();
    int[][] matrix2 = m2.getMatrix();
    if (matrix1 == null || matrix2 == null) {
      throw new NullPointerException("Матрицы незаполнены");
    }
    if (matrix1.length != matrix2.length || matrix1[0].length != matrix2[0].length) {
      throw new  NotNsizeMatrixException("Матрицы не одинакового размера");
    }
    result = new int[matrix1.length][matrix1[0].length];
    for (int i = 0; i < matrix1.length; i++) {
      for (int j = 0; j < matrix1[0].length; j++) {
        result[i][j] = matrix1[i][j] + matrix2[i][j];
      }
    }
    return result;
  }

  public static int[][] multiplication(Matrix m1, Matrix m2) throws NullPointerException, NotNsizeMatrixException {
    int[][] result;
    int[][] matrix1 = m1.getMatrix();
    int[][] matrix2 = m2.getMatrix();
    if (matrix1 == null || matrix2 == null) {
      throw new NullPointerException("Матрицы незаполнены");
    }
    if (matrix1[0].length != matrix2.length) {
      throw new  NotNsizeMatrixException("Не совпадают первое измерение м1 и второе измерение м2");
    }
    result = new int[matrix1.length][matrix2[0].length];
    for (int i = 0; i < matrix1.length; i++) {
      for (int j = 0; j < matrix2[0].length; j++) { // по столбцам м2
        for (int k = 0; k < matrix2.length; k++) {
          result[i][j] += matrix1[i][k] * matrix2[k][j];
        }
      }
    }
    return result;
  }
}
