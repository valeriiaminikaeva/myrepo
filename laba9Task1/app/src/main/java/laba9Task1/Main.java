package laba9Task1;

public class Main {
  public static void main(String[] args) throws NotNsizeMatrixException {
    //создаю 1 матрицу
    int rows1 = 4;
    int columns1 = 4;
    Matrix mtr1 = new Matrix(rows1, columns1); //создаю экземпляр
    for (int i = 0; i < mtr1.getMatrix().length; i++) { //параметры беру из геттера
      for (int j = 0; j < mtr1.getMatrix()[0].length; j++) {
        System.out.print(mtr1.getMatrix()[i][j] + " ");
      }
      System.out.println();
    }
    System.out.println();

    //создаю 1 матрицу
    int rows2 = 4;
    int columns2 = 4;
    Matrix mtr2 = new Matrix(rows2, columns2); //создаю экземпляр
    for (int i = 0; i < mtr2.getMatrix().length; i++) { //параметры беру из геттера
      for (int j = 0; j < mtr2.getMatrix()[0].length; j++) {
        System.out.print(mtr2.getMatrix()[i][j] + " ");
      }
      System.out.println();
    }
    System.out.println();

    //пробую задать матрицы вручную через сеттер
    Matrix mtr3 = new Matrix(2, 2);
    int[][] myNumbers = new int[][]{{8, 9}, {4, 6}};
    mtr3.setMatrix(myNumbers);
    for (int i = 0; i < mtr3.getMatrix().length; i++) {
        for (int j = 0; j < mtr3.getMatrix()[0].length; j++) {
            System.out.print(mtr3.getMatrix()[i][j] + " ");
        }
        System.out.println();
    }
    System.out.println();

    Matrix mtr4 = new Matrix(2, 3);
    int[][] myNumbers4 = new int[][]{{1, 0, 2}, {0, 5, 7}};
    mtr4.setMatrix(myNumbers4);
    for (int i = 0; i < mtr4.getMatrix().length; i++) {
        for (int j = 0; j < mtr4.getMatrix()[0].length; j++) {
            System.out.print(mtr4.getMatrix()[i][j] + " ");
        }
        System.out.println();
    }
    System.out.println();

    //сложение двух экземпляров

    int[][] sumRes = Actions.sum(mtr1, mtr2);
    for (int i = 0; i < sumRes.length; i++) {
        for (int j = 0; j < sumRes[0].length; j++) {
            System.out.print(sumRes[i][j] + " ");
        }
        System.out.println();
    }
    System.out.println();

    //умножение 2x2 и 2x3
    int[][] multiplicationRes = Actions.multiplication(mtr3, mtr4);
    for (int i = 0; i < multiplicationRes.length; i++) {
        for (int j = 0; j < multiplicationRes[0].length; j++) {
            System.out.print(multiplicationRes[i][j] + " ");
        }
        System.out.println();
    }
    System.out.println();

    // вычитание
    int[][] substractionRes = Actions.substraction(mtr1, mtr2);
    for (int i = 0; i < substractionRes.length; i++) {
        for (int j = 0; j < substractionRes[0].length; j++) {
            System.out.print(substractionRes[i][j] + " ");
        }
        System.out.println();
    }
    System.out.println();
  }
}
