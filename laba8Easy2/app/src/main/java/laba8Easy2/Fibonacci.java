package laba8Easy2;

import java.util.Scanner;
import java.util.*;
import java.lang.*;


class Fibonacci{

  public static int[] fibonacci(int a){
    int[] array = new int[a];
    if (a == 0){         //0 чисел - вывод null
      return null;
    }
    if (a == 1){        //1 число - вывод 0
      array[0] = 0;
      return array;
    }
    if (a == 2){        //2 числа - вывод 0,1
      array[0] = 0;
      array[1] = 1;
      return array;
    }
    if (a > 2){                //больше 2 чисел - считаем циклом
	    array[0] = 0;
      array[1] = 1;
      for (int i = 2; i < a; i++){
        array[i] = array[i-2] + array[i-1];
      }
    }
    return array;
  }

  //через рекурсию
  public static int fibonacciRec(int a, int[] arr){
   if (arr[a - 1] != 0){          //
     return arr[a - 1];
   }
   if (a <= 2){
     arr[a - 1] = a - 1;
     return arr[a - 1];
   }
   int result = fibonacciRec(a - 1, arr) + fibonacciRec(a - 2, arr);
   arr[a - 1] = result;
   return result;
  }

  public static int[] getFibonacci(int n){
    if (n == 0){
      return null;
    }
    int[] newArr = new int[n];
    fibonacciRec(n, newArr);
    return newArr;
  }

  public static void main(String[] args){
    int n = 30;
    System.out.println(Arrays.toString(getFibonacci(n)));

    int[] arrForFibo = new int[n];
    arrForFibo = fibonacci(n);
    System.out.println(Arrays.toString(arrForFibo));
  }
}
