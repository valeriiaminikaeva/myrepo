package laba8Easy2;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FibonacciTest{
  @Test void checkFiboWithoutRec(){
    int[] arr = new int[]{0, 1, 1, 2, 3, 5, 8, 13, 21, 34};
    assertArrayEquals(arr, Fibonacci.fibonacci(10), "checkFiboWithoutRec is incorrect");
  }

  @Test void checkFiboWithoutRec0(){
    assertArrayEquals(null, Fibonacci.fibonacci(0), "checkFiboWithoutRec is incorrect");
  }

  @Test void checkFiboWithRec(){
    int[] arr = new int[]{0, 1, 1, 2, 3, 5, 8, 13, 21, 34};
    assertArrayEquals(arr, Fibonacci.getFibonacci(10), "checkFiboWithRec is incorrect");
  }

  @Test void checkFiboWithRec0(){
    assertArrayEquals(null, Fibonacci.getFibonacci(0), "checkFiboWithoutRec0 is incorrect");
  }



}
