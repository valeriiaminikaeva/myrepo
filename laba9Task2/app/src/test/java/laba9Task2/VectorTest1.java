package laba9Task2;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

public class VectorTest1 {
  class CheckOutMethodsSumAndDifTwoVector{
    Vector v1;
    Vector v2;

    @BeforeEach
    void createVectors(){
      v1 = new Vector(20, 10, 4, 5);
      v2 = new Vector(10, 5, 15, 2);
    }
    @Test void sumTwoVectorTest() {
      Vector resultVector = Vector.sum(v1, v2);
      resultVector.print();
      assertEquals(resultVector.getXX1(), v1.getXX1() + v2.getXX1(), "XX1 in SumTwoVectorTest not correct");
      assertEquals(resultVector.getYY1(), v1.getYY1() + v2.getYY1(), "YY1 in SumTwoVectorTest not correct");
    }
    @Test void subTwoVectorTest() {
      Vector resultVector = Vector.sub(v1, v2);
      resultVector.print();
      assertEquals(resultVector.getXX1(), v1.getXX1() - v2.getXX1(), "XX1 in SubTwoVectorTest not correct");
      assertEquals(resultVector.getYY1(), v1.getYY1() - v2.getYY1(), "YY1 in SubTwoVectorTest not correct");
    }
  }
}
