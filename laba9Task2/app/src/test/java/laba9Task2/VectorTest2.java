package laba9Task2;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;



class VectorTest2 {
  @Test void checkingPrint() {
    Vector sampleVector = new Vector(5, 0, 10, 0);
    assertEquals("Начало вектора (5.0;0.0)" + "\n" +
                 "Конец вектора (10.0;0.0)" , sampleVector.print(),"print works incorrect");
  }

  @Test void checkingMultiVectorNum() {
    Vector sampleVector = new Vector(5, 15, 10, 20);
    Vector result = Vector.multi(sampleVector, 10);
    assertEquals("Начало вектора (50.0;150.0)" + "\n" + "Конец вектора (100.0;200.0)", result.print(),"checkingMultiVectorNum works incorrect");
  }

  @Test void checkingDivVectorNum() {
    Vector sampleVector = new Vector(5, 0, 10, 0);
    Vector result = Vector.div(sampleVector, 2);
    assertEquals("Начало вектора (2.5;0.0)" + "\n" + "Конец вектора (5.0;0.0)", result.print(),"checkingDiviVectorNum works incorrect");
  }
}
