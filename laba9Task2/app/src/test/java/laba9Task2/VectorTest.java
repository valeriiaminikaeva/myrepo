package laba9Task2;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

public class VectorTest {
  class Check1{
    Vector v1;
    Vector v2;

    @BeforeEach
    void makeVectors(){
      v1 = new Vector(20, 10, 4, 5);
      v2 = new Vector(10, 5, 15, 2);
    }
    @Test void addThisAndAnotherTest() {
      v1.add(v2);
      assertEquals(v1.getX(), 30, "X not correct AddThisAndAnotherTest");
      assertEquals(v1.getY(), 15, "Y not correct AddThisAndAnotherTest");
      assertEquals(v1.getX1(), 19, "X1 not correct AddThisAndAnotherTest");
      assertEquals(v1.getY1(), 7, "Y1 not correct AddThisAndAnotherTest");
    }
    @Test void subThisAndAnotherTest() {
      v1.substraction(v2);
      assertEquals(v1.getX(), -10, "X not correct SubThisAndAnotherTest");
      assertEquals(v1.getY(), -5, "Y not correct SubThisAndAnotherTest");
      assertEquals(v1.getX1(), 11, "X1 not correct SubThisAndAnotherTest");
      assertEquals(v1.getY1(), -3, "Y1 not correct SubThisAndAnotherTest");
    }
  }
}
