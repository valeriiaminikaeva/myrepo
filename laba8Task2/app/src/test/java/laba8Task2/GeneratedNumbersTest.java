package laba8Task2;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class GeneratedNumbersTest {
    @Test void tryShortArray(){
      Generator generator = new Generator(50, 5);
      generator.generate();
      assertEquals(true, generator.generatedInTime(), "short array not correct");
    }

    @Test void tryLongArray(){
      Generator generator = new Generator(2000000, 5);
      generator.generate();
      assertEquals(false, generator.generatedInTime(), "long array not correct");
    }

    @Test void ctryEmptyArray(){
      Generator generator = new Generator(0, 5);
      generator.generate();
      assertEquals(true, generator.generatedInTime(), "empty array not correct");
    }
}
