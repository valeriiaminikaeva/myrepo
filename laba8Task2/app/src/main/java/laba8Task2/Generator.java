package laba8Task2;
import java.util.Date;

class Generator{

  private int[] randomNumbers;
  private long timeToGenerate;
  private int timeMax;
  private int count;

  Generator(int count, int timeMax){
    this.count = count;
    this.timeMax = timeMax;
    this.randomNumbers = new int[this.count];
  }

  public void generate(){
    long start = new Date().getTime();
    for (int i = 0; i < this.count; i++){
      this.randomNumbers[i] = (int)(Math.random()*100);
    }
    this.timeToGenerate = (new Date().getTime() - start);
  }

  public boolean generatedInTime(){
    return this.timeToGenerate < this.timeMax;
  }

  public int[] getRandomNumbers(){
    return this.generatedInTime() ? this.randomNumbers : null;
  }
}
