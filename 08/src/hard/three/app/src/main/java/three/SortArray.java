package three;
import java.util.Scanner;
import java.util.Arrays;

public class SortArray{

  public static void sort() {
    double[] a = new double[5];
    int i = a.length - 1;
    while (i > 0){
      int j = 0;
      double k = 0;
      while (j < i){
        if (a[j] > a[j+1]){
          k = a[j];
          a[j] = a[j+1];
          a[j+1] = k;
        }
        j += 1;
      }
      i -= 1;
    }
    // for (int i = 1; i < a.length; i++) {
    //   System.out.print(a[i]);
    // }
  }


  public static double[] createSortedArray(double[] a){
    double[] array_new = a.clone();
    int i = array_new.length - 1;
    while (i > 0){
      int j = 0;
      double k = 0;
      while (j < i){
        if (array_new[j] > array_new[j+1]){
          k = array_new[j];
          array_new[j] = array_new[j+1];
          array_new[j+1] = k;
        }
        j += 1;
      }
      i -= 1;
    }
    return array_new;
  }

  public static void printArray(double[] array){
    int n = array.length;
    for (int i = 0; i< n; i++){
      System.out.print(array[i] + " ");
    }
    System.out.println();
  }

  public static void main(String[] args){
    double[] array = new double[]{3,2,4,6,5};
    printArray(createSortedArray(array));
    sort();
  }

}
