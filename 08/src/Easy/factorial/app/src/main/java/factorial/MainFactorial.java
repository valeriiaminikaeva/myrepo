package factorial;

public class MainFactorial{

    public static void main(String[] args){
        int n = -5;
        System.out.println(getFactorial(n));
    }

    public static int getFactorial(int n) {
        if(n <= 1) {
            return 1;
        }
        else{
            int result = 1;
            for (int i = 1; i <= n; i++) {
                result = result * i;
            }
            return result;
        }

    }
}
