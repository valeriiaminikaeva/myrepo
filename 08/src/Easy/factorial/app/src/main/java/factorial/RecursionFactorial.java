package factorial;

public class RecursionFactorial{

    public static void main(String[] args){
        int n = 7;
        System.out.println(getFactorial(n));
    }

    public static int getFactorial(int n) {
        if(n <= 1) {
            return 1;
        }

        else {
            return n * getFactorial(n - 1);
        }
    }
}
