package fibonucci;

class RecursionFibonucci{
	public static void main(String[] args){
        int n = 30;
        for(int i = 0; i < n; i++ ){
            System.out.printf("%d  ", fibonucciRec(i));
        }
    }

	public static int fibonucciRec(int a){
        if(a == 0){
            return 0;
        }
        if(a == 1){
            return 1;
        }
        else{
            return fibonucciRec(a-1) + fibonucciRec(a-2);
        }
	}
}
