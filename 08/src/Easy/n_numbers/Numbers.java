package n_numbers;

class Numbers{
    public static void main(String[] args) {
        int n = 5;
        int i = 0;
        int sum = 0;
        while (i<=n){
            sum += i;
            i++;
        }
        System.out.println(sum);
    }
}
