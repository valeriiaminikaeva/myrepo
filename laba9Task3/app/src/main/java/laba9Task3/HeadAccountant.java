package laba9Task3;

public class HeadAccountant extends Worker{

  HeadAccountant(String name, String position, int salary){
    super(name, position, salary);
  }

  public String countAccSalary(){
    return "I've given our accountants salary!";
  }
}
