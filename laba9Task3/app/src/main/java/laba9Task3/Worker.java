package laba9Task3;
//The class Object does not itself implement the interface Cloneable, so calling the clone method on an object whose class is Object will result in throwing an exception at run time.
public class Worker implements Cloneable{
  private String name;
  private String position;
  private int salary;

  Worker(String name, String position, int salary){
    this.name = name;
    this.position = position;
    this.salary = salary;
  }

  public String getName(){
    return this.name;
  }

  public void setName(String name){
    this.name = name;
}

  public String getPosition(){
    return this.position;
  }

  public void setPosition(String position){
    this.position = position;
  }

  public int getSalary(){
    return this.salary;
  }

  public void setSalary(int salary){
    this.salary = salary;
  }

//переопределение методов Object
  public String toString(){  //the toString method returns a string that "textually represents" this object. Можно добавить @ и хэшкод, если нужно
    return "Employee's name: " + this.name +
           ", position: " + this.position +
           ", salary: " + this.salary;
  }

  protected int myCipher(String a){ //для метода hashCode
    int d[] = new int[255];
    for (int i = 0; i < a.length(); i++) {
        d[i] = Integer.valueOf(a.charAt(i))*(100 - i);
    }
    int cypher = 0;
    for (int i = 0; i < d.length; i++){
      cypher += d[i];
    }
    return cypher;
  }

  public int hashCode() {  //Returns a hash code value for the object
    return myCipher(this.name) + myCipher(this.position);
  }

  public boolean equals(Object x) {
    //It is reflexive: for any non-null reference value x, x.equals(x) should return true.
    //for any non-null reference values x and y, x.equals(y) should return true if and only if y.equals(x) returns true.
    //this method returns true if and only if x and y refer to the same object (x == y has the value true
    if(this == x && this != null && x != null) {
      return true;
    }
    //For any non-null reference value x, x.equals(null) should return false.
    if(x == null || getClass() != x.getClass()){
      return false;
    }
    Worker worker = (Worker) x;
    return  this.salary == worker.salary &&
            this.position == worker.position &&
            this.name == worker.name;
  }

  public Object clone() throws CloneNotSupportedException { //Creates and returns a copy of this object.
    return (Worker) super.clone(); //By convention, the returned object should be obtained by calling super.clone. If a class and all of its superclasses (except Object) obey this convention, it will be the case that x.clone().getClass() == x.getClass().
  }
}
