package laba9Task3;

public class Main{

  public static void main(String[] args){
    int n = 4;
    Worker[] workers = new Worker[n];
    workers[0] = new Accountant("Tom", "Accountant", 40000);
    workers[1] = new HeadAccountant ("Jerry", "Head Accountant", 50000);
    workers[2] = new Engeneer ("Rick", "Engeneer", 60000);
    workers[3] = new Clerck ("Morty", "Clerck", 25000);

    for (int i = 0; i < workers.length; i++){
      System.out.println(workers[i].toString() + " ");
    }
  }
}
