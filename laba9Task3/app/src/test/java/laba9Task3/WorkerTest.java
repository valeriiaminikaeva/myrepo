package laba9Task3;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import static org.junit.jupiter.api.Assertions.*;


class WorkerTest {

  @Test void methodEqualsTest() throws CloneNotSupportedException{
    Worker worker1 = new Worker("Oleg", "Worker", 20000);
    Worker worker2 = new Worker("Oleg", "Worker", 20000);
		Worker worker3 = new Worker("Kuzya", "SuperWorker", 21000);
		assertTrue(worker1.equals(worker1), "equals() is incorrect");
		assertTrue(worker1.equals(worker2), "equals() inputs are not the same");
		assertTrue(!(worker1.equals(worker3)), "equals() is incorrect");
  }

	@Test void methodToStringTest(){
    Worker worker1 = new Worker("Oleg", "Worker", 20000);
		assertEquals(worker1.toString(), "Employee's name: Oleg, position: Worker, salary: 20000");
	}

	@Test void methodCloneTest() throws CloneNotSupportedException{
    Worker worker1 = new Worker("Oleg", "Worker", 20000);
    Worker worker2 = new Worker("Oleg", "Worker", 20000);
		boolean trueClone = (worker1.getName() == worker2.getName()) &&
												 (worker1.getPosition() == worker2.getPosition()) &&
												 (worker1.getSalary() == worker2.getSalary());
		assertTrue(trueClone, "clone() is incorrect");

	}

  @Test void methodHachCodeTest() {
    Worker worker1 = new Worker("Oleg", "Worker", 20000);
    Worker worker2 = new Worker("Oleg", "Worker", 20000);
    assertEquals(worker1.hashCode(), worker2.hashCode(), "methodHachCodeTest is incorrect");
  }

}
