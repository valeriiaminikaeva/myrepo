package laba9Task3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class HeadAccountantTest {
  HeadAccountant ha = new HeadAccountant("Anthony", "HeadAccountant", 50000);

  @Test void tryHeadAccountantCountAccSalary() {
    assertEquals("I've given our accountants salary!", ha.countAccSalary(), "tryHeadAccountantCountAccSalary is incorrect");
  }
}
