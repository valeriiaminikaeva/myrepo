package laba9Task3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AccountantTest {
  Accountant acc = new Accountant("Anthony", "Accountant", 50000);

  @Test void tryAccountantGiveSalary() {
    assertEquals("I've counted something!", acc.giveSalary(), "tryAccountantGiveSalary is incorrect");
  }
}
