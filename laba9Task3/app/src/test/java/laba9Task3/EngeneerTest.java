package laba9Task3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class EngeneerTest {
  Engeneer engeneer = new Engeneer("Anthony", "engeneer", 50000);

  @Test void tryEngeneerInvent() {
    assertEquals("I've invented something!", engeneer.invent(), "tryEngeneerInvent is incorrect");
  }
}
