package laba9Task4;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TriangleTest {
  @Test void GetTest() throws FigureNotExistException, WrongEdgesException, NotTriangleException{
    Triangle check = new Triangle(new int[]{7, 8, 9});
		assertEquals(check.get1Side(), 7, "get 1S incorrect");
		assertEquals(check.get2Side(), 8, "get 2S incorrect");
		assertEquals(check.get3Side(), 9, "get 3S incorrect");
  }
  
	@Test void NotTriangleTest() {
		assertThrows(NotTriangleException.class,() ->
    {new Triangle(new int[] {8, 9, 10, 11});});
	}
}
