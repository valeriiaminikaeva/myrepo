/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package laba9Task4;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FigureTest {
  @Test void GetEdgeTest() throws WrongEdgesException, FigureNotExistException{
    Figure check = new Figure(new int[]{10, 3, 3, 10});
		assertEquals(check.getEdge(1), 10, "getEdge is incorrect");
		assertThrows(WrongEdgesException.class, () ->
    {check.getEdge(5);},
    "Нет такой стороны");
  }

	@Test void CheckEdgesTest() throws FigureNotExistException{
		assertThrows(FigureNotExistException.class,() ->
    {new Figure(new int[] {5, 8, 10, 4, 100});},
    "checkEdges is incorrect");
	}
}
