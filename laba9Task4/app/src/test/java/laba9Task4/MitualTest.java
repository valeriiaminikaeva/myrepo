package laba9Task4;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MitualTest {

    @Test void CheckPrintText() {
      Text text = new Text("hahahaha!!!");
      String target1 = "Text: hahahaha!!!";
      assertEquals(target1, text.print(), "CheckPrintText is incorrect");
    }

    @Test void CheckPrintFigure() throws FigureNotExistException, WrongEdgesException{
      int[] arrForFigure = new int[]{3, 5, 3, 5};
      Figure figure = new Figure(arrForFigure);
      String target2 = "Figure\nNumber edges: " + 4 + "\nPerimeter: " + 16;
      assertEquals(target2, figure.print(), "CheckPrintFigure is incorrect");
    }

    @Test void CheckPrintTriangle() throws FigureNotExistException, WrongEdgesException, NotTriangleException{
      int[] arrForTriangle = new int[]{7,8,9};
      Figure triangle = new Triangle(arrForTriangle);
      String target3 = "Triangle: " + "\nPerimeter: " + 24;
      assertEquals(target3, triangle.print(), "CheckPrintTriangle is incorrect");
    }
}
