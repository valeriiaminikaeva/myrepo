package laba9Task4;

public class Figure extends Mutual{
	int[] edges;
	int perimetr;

	Figure(int[] edges) throws FigureNotExistException{
		this.edges = edges;
		findPerimetr();
		checkEdges();
	}

	public int getEdge(int a) throws WrongEdgesException{
		if (a <= edges.length && a > 0){   // если меньше, то только две стороны
			return edges[a-1];
		}
		else{
			throw new WrongEdgesException();
		}
	}

	public void findPerimetr(){
		for (int i = 0; i < edges.length; i++){
			perimetr += edges[i];
		}
	}

	public void checkEdges() throws FigureNotExistException{
		for (int i = 0; i < edges.length; i++){
			if (2 * edges[i] >= perimetr){
				throw new FigureNotExistException();
			}
		}
	}

	@Override
	public String print(){
		return  "Figure\nNumber edges: " + edges.length + "\nPerimeter: " + perimetr;
	}

	public void printArray(Figure[] figureArray){
		for (int i = 0; i < figureArray.length; i++){
			System.out.println(figureArray[i].print());
		}
	}
}
