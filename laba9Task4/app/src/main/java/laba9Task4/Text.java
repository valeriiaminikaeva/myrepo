package laba9Task4;

public class Text extends Mutual{
  String line;

  Text(String line){
    this.line = line;
  }

  public String getLine(){
    return this.line;
  }

  public void setLine(String line){
    this.line = line;
  }

  @Override
  public String print(){
    return "Text: " + line;
  }

  public void printArray(Text[] allArray){
    for(int i = 0; i < allArray.length; i++){
      allArray[i].print();
    }
  }
}
