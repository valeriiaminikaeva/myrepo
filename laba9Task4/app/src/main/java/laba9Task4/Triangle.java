package laba9Task4;

public class Triangle extends Figure{

	Triangle(int[] edges) throws NotTriangleException, FigureNotExistException{
		super(edges);
		if (edges.length != 3){
			throw new NotTriangleException();
		}
	}

	int get1Side() throws WrongEdgesException{
		return getEdge(1);
	}

	int get2Side() throws WrongEdgesException{
		return getEdge(2);
	}

	int get3Side()throws WrongEdgesException{
		return getEdge(3);
	}

	@Override
	public String print(){
		return  "Triangle: " + "\nPerimeter: " + perimetr;
	}
}
