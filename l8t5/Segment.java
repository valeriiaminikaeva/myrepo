package l8t5;
import java.util.Scanner;

public class Segment {
  public static void main(String[] args){
    System.out.println("Введите количество повторений, размер области и сдвиг относительно начала координат по очереди. Не вводите ничего, кроме целого числа, возможно, программист не успел это обработать)))");
    Scanner sc = new Scanner(System.in);
    int repeat = sc.nextInt(); //ввести 0 или больше
    int sizeOfTheCoordinatePlain = sc.nextInt(); //больше нуля нужно вводить
    int swing = sc.nextInt(); //проверка ниже
    int i = 0;
    int counter = 0;

    while(i < repeat){ //i++
      System.out.println("Генерация отрезков...");

      // в массив записать точки, где отрезок существует
      int start1 = (int)(Math.random()*sizeOfTheCoordinatePlain) + swing;
      int end1 = (int)(Math.random()*sizeOfTheCoordinatePlain +  start1) + swing;
      int[] segment1 = new int[end1 - start1 + 1];

      int j = 0;
      while(j < segment1.length){
        segment1[j] = start1;
        System.out.format("%d ", segment1[j]);
        start1++;
        j++;
      }

      System.out.println("------------------------------------------------");

      int start2 = (int)(Math.random()*sizeOfTheCoordinatePlain) + swing;
      int end2 = (int)(Math.random()*sizeOfTheCoordinatePlain +  start2) + swing;
      int[] segment2 = new int[end2 - start2 + 1];

      int k = 0;
      while(k < segment2.length){
        segment2[k] = start2;
        System.out.format("%d ", segment2[k]);
        start2++;
        k++;
      }

      //проверка на выход за пределы по сдвигу
      // boolean s1 = false;
      // boolean e1 = false;
      // boolean s2 = false;
      // boolean e2 = false;
      //
      // if (swing != 0){
      //   start1 = start1 + swing;
      //   if (start1 > 0){
      //     s1 = true;
      //   }
      //   else{
      //     s1 = false;
      //   }
      //   end1 = end1 + swing;
      //   if (end1 < sizeOfTheCoordinatePlain){
      //     e1 = true;
      //   }
      //   else{
      //     e1 = false;
      //   }
      //   start2 = start2 + swing;
      //   if (start2 > 0){
      //     s2 = true;
      //   }
      //   else{
      //     s2 = false;
      //   }
      //   end2 = end2 + swing;
      //   if (end2 < sizeOfTheCoordinatePlain){
      //     e2 = true;
      //   }
      //   else{
      //     e2 = false;
      //   }
      //
      //   if (s1 == true && e1 == true && s2 == true && e2 == true){
      //     System.out.println("Поиск пересечений отрезков...");
      //     continue;
      //   }
      //   else{
      //     System.out.println("Возможны ошибки в расчете из-за того, что отрезок/отрезки выходят за пределы координатной области. Попробуйте начать сначала и введите данные корректно.");
      //     break;
      //   }
      // }


      int a = 0;
      int b = 0;
      while(a < segment1.length){
        while(b < segment2.length){
          if(segment1[a] == segment2[b]){
            counter++;
          }
          b++;
        }
        a++;
      }
      System.out.println("Количество пересечений: " + counter);
      i++;
    }

  }
}
