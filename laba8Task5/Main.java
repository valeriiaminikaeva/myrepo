package laba8Task5;

import java.util.Scanner;
import static java.lang.Math.abs;

class Main {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("Введите количество повторений, размер области и сдвиг относительно начала координат по очереди");
    int repeat = sc.nextInt();
    int sizeOfCoordinateArea = sc.nextInt();
    int swing = sc.nextInt();

    Segment[] segments = new Segment[repeat];

    int max = swing + sizeOfCoordinateArea;
    int min = abs(sizeOfCoordinateArea - swing);
    int counter = 0;

    for (int i = 0; i < repeat; i++) {
      segments[i] = new Segment();
      segments[i].start1 = (int) (Math.random() * max) + min;
      segments[i].end1 = (int) (Math.random() * max) + min;
      segments[i].start2 = (int) (Math.random() * max) + min;
      segments[i].end2 = (int) (Math.random() * max) + min;
    }

    for (int i = 0; i < repeat; i++) {
      if ((segments[i].start1 > segments[i].start2 && segments[i].start1 < segments[i].end2) ||
          (segments[i].end1 > segments[i].start2 && segments[i].end1 < segments[i].end2) ||
          (segments[i].start1 < segments[i].start2 && segments[i].end1 > segments[i].end2) ||
          (segments[i].end1 > segments[i].end2 && segments[i].end1 < segments[i].start2) ||
          (segments[i].start1 > segments[i].end2 && segments[i].start1 < segments[i].start2) ||
          (segments[i].end1 < segments[i].end2 && segments[i].start1 > segments[i].start2)) {
        counter++;
      }
    }
    System.out.println(counter);
  }
}
