package laba8Easy6;

public class Diagonal{

    public static void main(String[] args) {

        int n = 10;
        int[][] a = new int[n][n];

        for(int i = 0; i < a.length; i++){
            for(int j = 0; j < a.length; j++){
                a[i][j] = n*i + j;
            }
        }

        for (int i = 1; i < n; i++) {
            for (int j = 0; j < i; j++) {
                System.out.printf("%4d", a[i][j]);
            }
            System.out.println();
        }
    }
}
