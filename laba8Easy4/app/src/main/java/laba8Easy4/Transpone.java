package laba8Easy4;

public class Transpone {
    public static void main(String[] args) {

        int n = 10;
        int[][] a = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = n*i + j;
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.printf("%3d", a[j][i]);
            }
            System.out.println();
        }
    }
}
