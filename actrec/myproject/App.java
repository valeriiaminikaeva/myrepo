package actrec.myproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.Properties;

public class App {
  private Connection connection;

    public Connection createConnection(String databaseUrl){
      Connection connection = DriverManager.getConnection(databaseUrl);

      return connection;
    }

    public static Connection getConnection(){
      if(connection == null) {
        connection = createConnection();
      }
      return connection;
    }

    public static ResultSet execute(String query){
      Statement stmt = null;
      ResultSet rs = null;
      Connetion conn = getConnection();

      try {
          stmt = conn.createStatement();
          ResultSet rs = stmt.executeQuery(query);
          while (rs.next()) {
            int total = rs.getInt("total");
            System.out.println(total);
          }
      } catch (SQLException e ) {
        System.out.println(e.getMessage());
      } finally {
        if (stmt != null) { stmt.close(); }
      }

      return rs;
    }

    public static String[] getAll() {
      return execute("SELECT * FROM *");
    }

    public static String getById(int id) {
      return execute("SELECT TOP 1 * FROM entity WHERE id = " + id)[0];
    }



    public static void main(String[] args) throws SQLException {
      Properties connectionProps = new Properties();
      connectionProps.put("user", "postgres");
      connectionProps.put("password", "123");
      String url = "jdbc:postgresql://127.0.0.1:5437/postgres";
      Connection conn = DriverManager.getConnection(url, connectionProps);
      System.out.println("Connected to database");

      Statement stmt = null;
      String query = "select 1 total";
    }


}
