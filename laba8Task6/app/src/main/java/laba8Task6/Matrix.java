package laba8Task6;
import java.util.Scanner;

public class Matrix {

  public static int[][][] makeMatrixes( int howMany, int rows){
    int[][][] matrix = new int[howMany][rows][rows]; //каждая матрица отдельно, внутри каждой 2 измерения. 1 матрица в 1 ячейке
    for (int i = 0; i < howMany; i++) {
      for (int j = 0; j < rows; j++) {
        for (int k = 0; k < rows; k++) {
          matrix[i][j][k] = (int)(Math.random() * 11);
        }
      }
    }
    return matrix;
  }

  public static int[][] multiplyMatrixes(int[][] matrix1, int[][] matrix2) {
    int[][] result = new int[matrix1.length][matrix2.length];
      for (int i = 0; i < matrix1[0].length; i++) {
        for (int j = 0; j < matrix2.length; j++) {
          for (int k = 0; k < matrix2[0].length; k++) {
            result[i][j] = result[i][j] + matrix1[i][k] * matrix2[k][j];
          }
        }
      }
      return result;
  }

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int howMany = sc.nextInt();
    int rows = sc.nextInt();
    long start = System.currentTimeMillis();
    int[][][] example = makeMatrixes(howMany, rows);
    for (int i = 0; i < howMany - 1; i++) { //-1 чтобы не выйти за границы
      example[i + 1] = multiplyMatrixes(example[i], example[i + 1]);
    }
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < rows; j++) {
        System.out.print(example[example.length - 1][i][j] + " ");
      }
      System.out.println();
    }
    System.out.println(System.currentTimeMillis() - start + "мс");
  }
}
