package laba8Task6;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MatrixesMultiplyTest {

  @Test void tryMultiplyMatrixes() {
    int[][] arrtotest1 = {{1,5}, {9,8}};
    int[][] arrtotest2 = {{0,6}, {4,9}};
    int[][] target = {{20,51},{32,126}};
    assertArrayEquals(target, Matrix.multiplyMatrixes(arrtotest1, arrtotest2));
  }

  @Test void tryMultiplyNullMatrixes() {
    int[][] arrtotest1 = {{0}};
    int[][] arrtotest2 = {{0}};
    int[][] target = {{0}};
    assertArrayEquals(target, Matrix.multiplyMatrixes(arrtotest1, arrtotest2));
  }

  @Test void tryMultiplyByOnelMatrixes() {
    int[][] arrtotest1 = {{1}};
    int[][] arrtotest2 = {{1}};
    int[][] target = {{1}};
    assertArrayEquals(target, Matrix.multiplyMatrixes(arrtotest1, arrtotest2));
  }
}
