package l8Easy1;
import java.util.Scanner;

public class Factorial{

  public static long fctrlRec(long a){
    if (a==1){
      return 1;
    }
    return a*fctrlRec(a-1);
  }

  //метод без рекурсии
  public static long fctrl(long a){
    long fctrl = 1;
    for (long i = 1; i <= a; i++){
      fctrl *= i;
    }
    return fctrl;
  }



  public static void main(String[] args){
  Scanner sc =  new Scanner(System.in);
  System.out.println("Введите число, для которого нужно вычислить факториал:");
  long number = sc.nextLong();
  System.out.println(fctrl(number));
  System.out.println(fctrlRec(number));

  }
}
