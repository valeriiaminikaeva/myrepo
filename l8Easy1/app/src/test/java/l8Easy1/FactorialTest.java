package l8Easy1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FactorialTest {
    @Test void checkFctrlWithRec(){
      assertEquals(24, Factorial.fctrlRec(4), "checkFctrlWithRec is incorrect");
    }

    @Test void checkFctrlWithRecThrough1(){
      assertEquals(1, Factorial.fctrlRec(1), "checkFctrlWithRecThrough1 is incorrect");
    }

    @Test void checkFctrlWithoutRec(){
      assertEquals(24, Factorial.fctrl(4), "checkFctrlWithoutRec is incorrect");
    }

    @Test void checkFctrlWithoutRecThrough1(){
      assertEquals(1, Factorial.fctrl(1), "checkFctrlWithoutRecThrough1 is incorrectThrough1");
    }


}
