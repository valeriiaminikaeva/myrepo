package testproject;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
    @Test void change(){
      
      int[] arr1 = new int[] {1, 2, 3, 4};
      int[] arr2 = App.change(arr1);
      assertEquals(arr1[0], arr2[arr2.length - 1], "first correct");
      assertEquals(arr2[0], arr1[arr1.length - 1], "last correct");
    }
    
    @Test void completion(){
      int[] arr1 = new int[] {1, 2, 3, 4};
      int[] arr2 = App.completion(arr1);
      assertEquals(arr1[0], arr2[0], "odd cor");
      assertEquals(0, arr2[1], "even cor");
      assertEquals(arr1[2], arr2[2], "odd cor");
      assertEquals(0, arr2[3], "even cor");
      
    }
}
