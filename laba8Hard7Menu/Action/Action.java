package laba8Hard7Menu.Action;

abstract public class Action{
  String message;

  Action(String message){
    this.message = message;
  }

  abstract public void act()  throws Exception;

  public String getMessage(){
    return this.message;
  }  
}
