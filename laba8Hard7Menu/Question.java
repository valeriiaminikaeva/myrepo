package laba8Hard7Menu;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;
import laba8Hard7Menu.Action.*;

public class Question{
  ArrayList<Question> children = new ArrayList<Question>(); // список детей
  Question parent; //родитель
  ArrayList<Action> actions = new ArrayList<Action>(); //список действий

  public void doTask(Action action){ //добавить конкретный Action в список actions
    this.actions.add(action);
  }

  public void goNext(Question nextLevel){   //создаем связь родитель-дети (этот уровень - след уровень)
    this.children.add(nextLevel); //  в список детей этого Question(от чего вызывается метод) добавляется след уровень
    nextLevel.parent = this; //след уровню в родителя ставится то, от чего вызывается метод
  }

  public void consolOutput(){   //взаимодействие с пользователем через консоль  
    System.out.println("Вот, что тут можно сделать:");
    if (this.parent == null){     //если нет родителей, то это корень => вернуться на уровень назад нельзя, поэтому можно только выйти
      System.out.println("0 Выйти");
    }
    else{
      System.out.println("0 Вернуться на уровень назад"); //если родитель есть, то можно вернуться на уровень назад
    }
    int num = 1; //уровень в дереве(пока в корне)
    for (int i = 0; i < this.children.size(); i++) {    //size - метод эррэйлиста, показ. его размер/длину
      System.out.println(num + " " + "Перейти на следующий уровень");
      num++;
    }
    for (int i = 0; i < this.actions.size(); i++) {
      System.out.println(num + " " + this.actions.get(i).getMessage());  //в списке действий от этого Question получаем i-тое действие, от него смотрим сообщение (оюращение к полю класса)
      num++;
    }
    System.out.println();
  }
}
