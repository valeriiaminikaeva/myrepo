package laba8Easy5;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class SymbolFinderTest {
    @Test void tryFindSymbol(){
      assertEquals(5, SymbolFinder.findSymbol("jsdgggfgkuagd", 'g'), "tryFindSymbol works incorrect");
    }

    @Test void tryFindSymbolInEmptyString(){
      assertEquals(0, SymbolFinder.findSymbol("", 'z'), "tryFindSymbolInEmptyString works incorrect");
    }

    @Test void tryFindSymbolWhenNoChar(){
      assertEquals(0, SymbolFinder.findSymbol("okokafaf", 'q'), "tryFindSymbolWhenNoChar works incorrect");
    }
}
