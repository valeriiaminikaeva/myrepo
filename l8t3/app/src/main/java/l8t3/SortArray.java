package l8t3;
import java.util.Scanner;
import java.util.Arrays;

public class SortArray{

  public static void sort(double[] arr) {
    for (int i = 1; i < arr.length; i++) {
      double key = arr[i];
      int j = i - 1;
      while (j >= 0 && key < arr[j]) {
        arr[j + 1] = arr[j];
        j--;
      }
      arr[j + 1] = key;
    }
    for (int i = 0; i < arr.length ;i++ ) {
      System.out.print(arr[i] + ",");
    }
  }

  public static double[] createSortedArray(double[] a){
    double[] res = a.clone();
    sort(res);
    return res;
  }

  public static void main(String[] args){
    double[] array = new double[]{3,2,4,6,5};
    createSortedArray(array);
    sort(array);
  }

}
