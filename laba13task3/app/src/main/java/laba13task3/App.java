package laba13task3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;

/**
  * Этот класс заменяет упоминания плохих слов
  * на любимые хорошие слова
  * @author Minikaeva Valeriia
  */
public class App {
    /**
    * Метод заменяет плохие слова в тексте на хорошие
    * @params text - текст со словами
    * @params badWords - плохие слова, которые нужно заменить
    * @params goodWord - слово, которым заменяются плохие слова
    * @return метод возвращает текст, в котором плохие слова заменены на хорошие
    */
    public static String replaceBadWords(String text, String[] badWords, String goodWord) {
        return Pattern.compile(String.join("|", badWords)).
                        matcher(text).replaceAll(goodWord);
    }

    /**
    * точка входа
    */
    public static void main(String[] args) {
      String text = "люблю питон";
      String[] badWords = new String[]{"питон"};
      String goodWord = "джава";
      System.out.println(replaceBadWords(text, badWords, goodWord));
    }

}
