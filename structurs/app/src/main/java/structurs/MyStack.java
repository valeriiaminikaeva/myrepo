package structurs;

import java.util.*;

public class MyStack<E> implements Collection<E> {
    private E[] stack;
    private int top;


    public MyStack(int size) {
        this.stack = (E[]) new Object[size];
        this.top = -1;
    }


    private MyStack(E[] stack) {
        this.stack = stack;
    }

    public void push(E element) {
        add(element);
    }


    public int pop() {
        return (int) stack[top--];
    }

    public int peek() {
        return (int) stack[top];
    }

    public boolean full() {
        return (top == stack.length - 1);
    }

    public boolean empty() {
        return (top == -1);
    }

    @Override
    public int size() {
        return top + 1;
    }


    @Override
    public boolean isEmpty() {
        return empty();
    }


    @Override
    public boolean contains(Object o) {
        for (int i = 0; i <= top; i++) {
            if (stack[i] == o) {
                return true;
            }
        }
        return false;
    }

    public E remove() {
        if (!isEmpty()) {
            E o = stack[top];
            top--;
            return o;
        } else {
            throw new NoSuchElementException();
        }
    }

    public Iterator iterator() {
        return new Iterator<E>() {
            private int nextIndex = 0;
            @Override
            public boolean hasNext() {
                return (nextIndex <= top - 1);
            }
            @Override
            public E next() {
              E stackValue = stack[nextIndex];
              nextIndex ++;
              return stackValue;
            }
          };
      }


    @Override
    public Object[] toArray() {
        return Arrays.copyOf(stack, top + 1);
    }



    @Override
    public Object[] toArray(Object[] arr){
        throw new UnsupportedOperationException
        ("Object[] toArray(Object[] arr)");
    }



    @Override
        public boolean add(E e) {
            if ((top + 1) == stack.length) {
                expand(stack.length * 2);
        }
        top++;
        stack[top] = e;
        return true;
    }



    private void expand(int newSize) {
        stack = Arrays.copyOf(stack, newSize);
    }


    @Override
    public boolean remove(Object o) {
        for (int i = 0; i < size(); i++) {
            if( stack[i] == o) {
                for (int j = i; j < size() - 1; j++) {
                    stack[j] = stack[j + 1];
                }
                top--;
                return true;
            }
        }
        return false;
    }


    @Override
    public boolean containsAll(Collection c) {
        for (Object element : c) {
            if (!contains(element)) {
                return false;
            }
        }
        return true;
    }



    @Override
    public boolean addAll(Collection c) {
        for (Object element:  c) {
            add((E) element);
        }
        return true;
    }



    @Override
    public boolean removeAll(Collection c) {
        for (Object element : c) {
              remove(element);
        }
        return true;
    }


    @Override
    public boolean retainAll(Collection c) {
        for (Object element : stack) {
            if (!c.contains(element)) {
            remove(c);
            }
        }
        return true;
    }


    @Override
    public void clear() {
        this.top = -1;
    }


}
