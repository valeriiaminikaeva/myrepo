import org.junit.jupiter.api.Test;
import java.io.IOException;
import static org.junit.jupiter.api.Assertions.*;


class AppTest {
    @Test
    void TestValidateCorrectNumbers() {
      assertTrue(App.check("89181112233"), "89181112233");
      assertTrue(App.check("+79181112233"), "+79181112233");
      assertTrue(App.check("+7-9181112233"), "+7-9181112233");
      assertTrue(App.check("+7918-1112233"), "+7918-1112233");
      assertTrue(App.check("+7918111-2233"), "+7918111-2233");
      assertTrue(App.check("+7-918-111-22-33"), "+7-918-111-22-33");
    }

    @Test
    void TestValidateLetterInNumber() {
      assertTrue(!App.check("t-(918)-111-22-33"), "t-(918)-111-22-33");
      assertTrue(!App.check("tt-(918)-111-22-33"), "tt-(918)-111-22-33");
      assertTrue(!App.check("+7-(ttt)-111-22-33"), "+7-(ttt)-111-22-33");
      assertTrue(!App.check("+7-(918)-ttt-22-33"), "+7-(918)-ttt-22-33");
      assertTrue(!App.check("+7-(918)-111-tt-33"), "+7-(918)-111-tt-33");
      assertTrue(!App.check("+7-(918)-111-22-tt"), "+7-(918)-111-22-tt");
      assertTrue(!App.check("t7-(918)-111-22-33"), "t7-(918)-111-22-33");
      assertTrue(!App.check("+7t(918)-111-22-33"), "+7t(918)-111-22-33");
      assertTrue(!App.check("+7-(918)t111-22-33"), "+7-(918)t111-22-33");
      assertTrue(!App.check("+7-(918)-111t22-33"), "+7-(918)-111t22-33");
      assertTrue(!App.check("+7-(918)-111-22t33"), "+7-(918)-111-22t33");
      assertTrue(!App.check("+7-t918t-111-22-33"), "+7-t918t-111-22-33");
    }

    @Test
    void TestValidateMoreDigits() {
      assertTrue(!App.check("891811122334"));
    }

    @Test
    void TestValidateFewerDigits() {
      assertTrue(!App.check("8918111223"));
    }

    @Test
    void TestValidateMoreDigitsInDifferentPlaces() {
      assertTrue(!App.check("+7-(918)-1111-22-33"), "+7-(918)-1111-22-33");
      assertTrue(!App.check("+7-(918)-111-222-33"), "+7-(918)-111-222-33");
      assertTrue(!App.check("+7-(918)-111-22-333"), "+7-(918)-111-22-333");
      assertTrue(!App.check("+7-(9184)-111-22-33"), "+7-(9184)-111-22-33");
      assertTrue(!App.check("+72-(918)-111-22-33"), "+72-(918)-111-22-33");
    }
}
