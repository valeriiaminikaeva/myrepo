/*
 * Определить, является ли строка номером телефона.
 * Обязательно использование следующих конструкций: группы, квантификаторы, классы символов, экранирование, символы местоположения.
 */

package phonenumber;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс входа
 * @autor Миникаева Валерия
*/
public class App {
    /** Поле регулярное выражение для проверки на корректность номера телефона */
    private static final String REGEX = "(^8|^7|^\\p{Punct}??7)\\p{Punct}??9\\d{2}\\p{Punct}{0,2}?\\d{3}\\p{Punct}??\\d{2}\\p{Punct}??\\d{2}";
    private static final String EXIT_STRING = "e";
    /**
  * точка входа
  * @param args - массив входных строк
  */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String phone = sc.nextLine();
        while (!phone.equals(EXIT_STRING)){
            phone = sc.nextLine();
            System.out.println("This string is a phone: " + check(phone));
        }
    }

  /**
   * метод проверки на корректность номера телефона
   * @param phone - строка с номером телефона
   * @return - возвращает булевое значение о кореекности номера телефона
   */
    static boolean check(String phone){
      return Pattern.compile(REGEX).matcher(phone).matches();
    }
}
