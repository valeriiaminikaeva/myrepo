package regex2;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Scanner;


/**
  * Класс App для вывода всех существительных, к которым относится пятый/пятая и т.д.
  * @author Валерия Миникаева
  */

public class App {
  /**
    * метож нахождения существительных, которые являются "пятыми"
    * @param text - строка с текстом
    * @return - возвращает массив строк "пятых" существительных
    */
    public static String findFifth(String text) {
        String regex = "(\\П|п)\\ят(ый|ая|ое|ые|ого|ому|ом|ой|ую|ою|ых|ым|ыми|ью)\\s" +
        "(\\w+)";
        String result = "";
        Pattern pattern = Pattern.compile(regex, Pattern.UNICODE_CHARACTER_CLASS);
        Matcher matcher = pattern.matcher(text);

        while(matcher.find()) {
          result += matcher.group(3) + " ";
        }
        return result;
    }

    /**
    * точка входа
    * @param args - массив входных строк
    */
    public static void main(String[] args) {
      String text = "Пятую минуту пятью ребятами пятый колодец открывают";
      System.out.println(findFifth(text));
    }
}
